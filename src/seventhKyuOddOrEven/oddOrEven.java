package seventhKyuOddOrEven;

public class oddOrEven {
    public static String oddOrEven (int[] array) {
        // your code
        int sum = 0;
        for (int i = 0; i < array.length; i = i + 1) {
            sum = sum + array[i];
        }
        if (sum % 2 == 0) {
            return "even";
        } else {
            return "odd";
        }
    }
}