package eightKyuSumOfPositive;

public class Positive {
    public static int sum(int[] arr){
        int sum = 0;
        for (int s : arr) {
            if (s > 0) {
                sum += s;
            }
        }
        return sum;
    }

}